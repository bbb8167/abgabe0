---
title: "Demo"
author: "Team Foo"
date: "2022-10-31"
output: 
   html_document:
     keep_md: true
     toc: true
     toc_float: true
     number_sections: true
     theme: journal
---

# Header1
## Header2
### Header3

* Item 1
* Item 2
   + Item 2a
   + Item 2b
   
This is text, but always remember that  
a new line requires at least two empty spaces before shift.




```r
knitr::opts_chunk$set(echo = TRUE)
```

```r
vettoreA <- c(1,2,3)
vettoreB <- c(1,3,6)
vettoreA/vettoreB
```

```
## [1] 1.0000000 0.6666667 0.5000000
```




## R Markdown

This is an R Markdown document. Markdown is a simple formatting syntax for authoring HTML, PDF, and MS Word documents. For more details on using R Markdown see <http://rmarkdown.rstudio.com>.

When you click the **Knit** button a document will be generated that includes both content as well as the output of any embedded R code chunks within the document. You can embed an R code chunk like this:


```r
summary(cars)
```

```
##      speed           dist       
##  Min.   : 4.0   Min.   :  2.00  
##  1st Qu.:12.0   1st Qu.: 26.00  
##  Median :15.0   Median : 36.00  
##  Mean   :15.4   Mean   : 42.98  
##  3rd Qu.:19.0   3rd Qu.: 56.00  
##  Max.   :25.0   Max.   :120.00
```

## Including Plots

You can also embed plots, for example:

![](ps1-loesungen_files/figure-html/pressure-1.png)<!-- -->

Note that the `echo = FALSE` parameter was added to the code chunk to prevent printing of the R code that generated the plot.
